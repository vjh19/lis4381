> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4381

## Victoria Hagen

### Project 2 Requirements:

*Three items:*

1. Create an edit_petstore.php, edit_petstore_process.php and delete_petstore.php after duplicated sever-side validation from A5.
2. Add server-side validation and regular expressions.
3. Include screenshots of a before and after successful edit, failed validation, delete prompt, and successfully deleted record.

#### README.md file should include the following items:

* Course title, your name, assignment requirements, as per A1.
* Screenshots of list under item 3 in requirements.
* Link to local lis4381 web app: http://localhost/repos/lis4381/ .

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

|*Screenshot of Home Page:*|
|---|
|![Home](img/home.png)|

| *P2 index.php:*  | *edit_petstore.php:*  | 
|---|---|
| ![index.php](img/index.png)  |  ![edit_petstore.php](img/edit.png)  |

| *Failed Validation:* | *Passed Valdidation:*  | 
|---|---|
| ![failed](img/failed.png)  |  ![Passed Valdidation](img/passed.png)  |

| *Delete Record Prompt:* | *Sucessfully Deleted Record:*  | 
|---|---|
| ![delete prompt](img/delete.png)  |  ![deleted record](img/deleted.png)  |

|*RSS FEED:*|
|---|
|![RSS FEED](img/rss.png)|


#### Site Link:

*Portfolio Site:*
[Victoria Hagen Portfolio](http://localhost/repos/lis4381/index.php "Portfolio site")