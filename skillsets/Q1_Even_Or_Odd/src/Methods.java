import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program evaluates integers as even or odd.");
        System.out.println("Note: Program does *not* check for non-numeric characters.");

        System.out.println();
    }
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int num = 0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        num = sc.nextInt();
        sc.close();

        if(num % 2 != 0)
        {
            System.out.println(num + " is an odd number.");
        }
        else 
        {
            System.out.println(num + " is an even number.");

        }
    }
}