import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program evaluates largest of three integers");
        System.out.println("Note: Program checks for non-integer values and non-numeric values.");

        System.out.println();
    }
   //nonvalue-returning method (without object - static)
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int num =0, num2=0, num3=0;
        System.out.print("Please enter first number: ");
        Scanner sc = new Scanner(System.in);

        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter first number: ");
        }
        num = sc.nextInt();

        
        System.out.print("\nPlease enter a second number: ");

        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter second number: ");
        }
        num2 = sc.nextInt();


        System.out.print("\nPlease enter a third number: ");
  
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter third number: ");
        }
        num3 = sc.nextInt();

        sc.close();


        System.out.println();
        if(num > num2 && num > num3)
        {
            System.out.println("First is largest number");
        }
        else if(num2 > num && num2 >num3)
        {
            System.out.println("Second is largest number");

        }
        else if(num3 > num && num3 >num2)
        {
            System.out.println("Third is largest number");

        }
        else{
            System.out.println("Integers are equal.");
        }
    }
}