import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters: W or w, C or c, H or h, N or n.");
        System.out.println("Use following decision structures: if...else, and switch.");
        System.out.println();
    }
   //nonvalue-returning method (without object - static)
    public static void evaluatePhone()
    {
  
        
        System.out.println("Phone types:  W or w (work), C or c (cell), H or h (home), N or n (none).");
        System.out.print("Enter phone type: ");
        Scanner sc = new Scanner(System.in);
        char decision = sc.next().charAt(0);
        
        System.out.println("\nif...else: ");
        if(decision == 'C' || decision == 'c'){
            System.out.println("Phone type: cell");
        }
        else if(decision == 'W' || decision == 'w'){
            System.out.println("Phone type: work");
        }
        else if(decision == 'H'|| decision == 'h'){
            System.out.println("Phone type: home");
        }
        else if(decision == 'N' || decision == 'n'){
            System.out.println("Phone type: none");
        }
        else{
            System.out.println("Incorrect character entry.");
        }
        
        System.out.println("\nswitch: ");
        switch (decision) {
            case 'C':
            case 'c':
            System.out.println("Phone type: cell");
                break;
            case 'W':
            case 'w':
            System.out.println("Phone type: work");
                break;
            case 'H':
            case 'h':
            System.out.println("Phone type: home");
                break;
            case 'N':
            case 'n':
            System.out.println("Phone type: none");
                 break;
            default:
            System.out.println("Incorrect charcter entry");
                break;
        }
        System.out.println();
        sc.close();
    }
}