import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program evaluates largest of two integers.");
        System.out.println("Note: Program does *not* check for non-numeric charcters or non-integer values.");

        System.out.println();
    }
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int num1 = 0;
        int num2 = 0;
        System.out.print("Enter first integer: ");
        Scanner sc = new Scanner(System.in);
        num1 = sc.nextInt();
        System.out.print("Enter second integer: ");
        num2 = sc.nextInt();
        System.out.println();
        sc.close();

        if(num1 > num2)
        {
            System.out.println(num1 + " is larger than "+ num2);
        }
        if(num2 > num1)
        {
            System.out.println(num2 + " is larger than "+ num1);
        }
        else{
            System.out.println("Integers are equal.");
        }
    }
}