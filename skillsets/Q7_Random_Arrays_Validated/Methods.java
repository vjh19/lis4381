import java.util.*;
import java.util.Scanner;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Print minimum and maxium integer values.");
        System.out.println("Program prompts user to enter desired number of pseudorandom-generated integers (min 1).");
        System.out.println("Use following loop structures: for, enhance for, while, do...while.");
        System.out.println();
        System.out.println("Integer.MIN_VALUE = "+ Integer.MIN_VALUE);
        System.out.println("Integer.MAX_VALUE = "+ Integer.MAX_VALUE);
        System.out.println();
    }

    public static void evaluateLoop()
    {


        int desiredNum=1;
        Scanner sc = new Scanner(System.in);
        System.out.print("Enter desired number of pseudorandom-generated integers (min 1): ");
        
        
        while(!sc.hasNextInt())
        {
            System.out.println("Not valid integer!\n");
            sc.next();
            System.out.print("Please try again. Enter valid integer (min 1): ");
        }
        desiredNum= sc.nextInt();

        while(desiredNum<1)
        {
            System.out.print("\nNumber must be greater than 0. Please enter integer greater than 0: ");
            while(!sc.hasNextInt()){
                System.out.print("\nNumber must be an integer: ");
                sc.next();
                System.out.print("Please try again. Enter integer value greater than 0: ");

            }
            desiredNum= sc.nextInt();
        }
   
        Random random = new Random();

        System.out.println("for loop:");
        for(int i=1;i<=desiredNum;i++){
            int number = random.nextInt();
            System.out.println(number);  
        }
        System.out.println();

        
        int [] nums = random.ints(desiredNum).toArray();
        System.out.println("Enhanced for Loop:");
        for( int element: nums)
        {
            System.out.println(element);
        }
        System.out.println();

        System.out.println("while loop:");
        int f=0;
        while(f<desiredNum)
        {
            int number = random.nextInt();
            System.out.println(number); 
            f++;
        }
        System.out.println();

        System.out.println("do...while loop:");
        int z = 0;
        do
        {
         int number = random.nextInt();
            System.out.println(number); 
         z++;
        }while(z<desiredNum);
      sc.close();  
    }
 
    
    }
   