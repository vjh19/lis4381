import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program converts user-entered temperatures into Fahrenheit or Celsius scales.");
        System.out.println("Program continues to prompt user for entry until no longer requested");
        System.out.println("Note: upper or lower case letter permitted. Though, incorrect entries are not permitted.");
        System.out.println("Note: Program does not validate numeric data (optional requirement).");

    }

    public static void evaluateTemp()
    {   String answer = "y";
        double temp= 0.0;
        Scanner sc = new Scanner(System.in);
        while(answer.equals("y")){
            System.out.print("\nFarenheit to Celsius? Type \"f\", or Celsius to Farenheit? Type \"c\": ");
            String tempType = " ";
            tempType= sc.nextLine().toLowerCase();
        switch (tempType){
            case "f":
            System.out.print("Enter temperature in Fahrenheit: ");
            temp = sc.nextDouble();
            temp = ((temp - 32)*5)/9;
            System.out.println("Temperature in Celsius: "+ temp);
                break;
            case "c":
            System.out.print("Enter temperature in Celsius: ");
            temp = sc.nextDouble();
            temp = (temp*9/5)+32;
            System.out.println("Temperature in Fahrenheit: "+ temp);
            break;
            default:
            System.out.println("Incorrect entry. Please try again.");
            break;
            }
            System.out.print("\nDo you want to convert temperature (y or n)? ");
            answer = sc.next().toLowerCase();
            sc.nextLine();
            
            }sc.close();

            System.out.println("\nThank you for using our Temperature Conversion Program!");
            
        }
  
    }
   
