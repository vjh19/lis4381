import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program determines whether user-entered value is alpha, numeric, or special character.");
        System.out.println();
        System.out.println("References: \nASCII Background: https://en.wikipedia.org/wiki/ASCII \nASCII Character Table: https://www.ascii-code.com/ \nLookup Tables https://www.lookuptables.com/ ");
        System.out.println();
    }

    public static void evaluateValue()
    {   
        Scanner sc = new Scanner(System.in);
    System.out.print("Enter character: ");
    char value = sc.next().charAt(0);
    int ascii = (int)value;
    boolean alpha = Character.isAlphabetic(value);
    boolean numeric = Character.isDigit(value);
    boolean space = Character.isSpaceChar(value);
        if(alpha==true){
            System.out.println(value+" is alpha. " +"ASCII value: "+ ascii);
        }
        if(numeric==true){
            System.out.println(value+" is numeric. " +"ASCII value: "+ ascii);
        }
        if(numeric==false && alpha==false && space==false){
            System.out.println(value+" is special character. " +"ASCII value: "+ ascii);
        }
        System.out.println();
        sc.close();
}  

}