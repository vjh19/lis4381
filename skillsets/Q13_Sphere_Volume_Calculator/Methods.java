import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program calculates sphere volume in liquid U.S. gallons from user-entered diameter value in inches, and rounds to two decimal places.");
        System.out.println("Must use Java's *built-in* PI and pow() capabilities.");
        System.out.println("Program checks for non-integers and non-numeric values.");
        System.out.println("Program continues to prompt user entry until no longer requested, prompt accepts upper or lower case letters.");
        System.out.println();
    }

    public static void calculate()
    {   double volume = 0.0;
        double gallons = 0.0;
        int diameter = 0;
        char decision = ' ';
        Scanner sc = new Scanner(System.in);
        do{
            System.out.print("Please enter diameter in inches: ");
            while(!sc.hasNextInt()){
                System.out.println("Not a valid integer!\n");
                sc.next();
                System.out.print("Please try again. Enter diameter in inches: ");
            }
            diameter=sc.nextInt();
            System.out.println();

            volume =((4.0/3.0)*Math.PI *Math.pow(diameter/2.0, 3));
            gallons = volume/231;
            System.out.println("Sphere volume: "+ String.format("%,.2f", gallons) + " liquid U.S. gallons");

            System.out.print("\nDo you want to calculate another sphere volume (y or n)? ");
            decision = sc.next().charAt(0);
            decision = Character.toLowerCase(decision);

        }while(decision=='y');

            sc.close();

            System.out.println("\nThank you for using Sphere Volume Calculator!");
            
        }
  
    }
   
