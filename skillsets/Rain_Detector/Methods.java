import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("\"Cloud Computing:\"Yell at the clouds, make it *not* rain during the weekends!!");
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Rain Detector");
       
        System.out.println();

        System.out.println("Program Requirements:\n1. Capture user-entered input.\n2. Based upon day of week, determine if rain will occur.\n3. Note: Program does case-insensitive comparison.");
    }
   //nonvalue-returning method (without object - static)
    public static void evaluateWeather()
    {
        System.out.println();
        System.out.println("Input:");
        System.out.print("Enter full name of day of week: ");
        Scanner sc = new Scanner(System.in);
        String decision = sc.nextLine().toLowerCase();
        
        System.out.println();
        System.out.println("Output: ");
      
        switch (decision){
            case "saturday":
                System.out.println("Whoopee! It's the weekend!!!");
                break;
            case "friday":
                System.out.println("Whoopee! It's the weekend!!!");
                break;
            case "sunday":
                System.out.println("Whoopee! It's the weekend!!!");
                break;
            case "monday":
                System.out.println("Better get an umbrella!");
                break;
            case "tuesday":
                System.out.println("Better get an umbrella!");
                break;
            case "wednesday":
                System.out.println("Better get an umbrella!");
                break;
            case "thursday":
                System.out.println("Better get an umbrella!");
                break;
            default:
                System.out.println("Not a day of the week.");
                break;

        }
        System.out.println();
        sc.close();
        }
        
    }
