import java.util.*;

 class Methods
{
    static final Scanner sc = new Scanner(System.in);
    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("1) Program creates array size at run-time.");
        System.out.println("2) Program dispalys array size.");
        System.out.println("3) Program rounds sum and average of number to two decimal places.");
        System.out.println("4) Numbers *must* be float data type, not double.");
        System.out.println();
    }
   //nonvalue-returning method (without object - static)
    public static int validateArraySize()
    {
        int arraySize = 0;

        System.out.print("Please enter array size: ");

        while(!sc.hasNextInt())
        {
        System.out.println("Not a valid integer!\n");
        sc.next();
        System.out.print("Please try again. Enter array size: ");  
        }
        arraySize=sc.nextInt();
        System.out.println();

        return arraySize;
    }
    public static void calculateNumbers(int arraySize){
        float sum = 0.0f;
        float average = 0.0F;

        System.out.println("Please enter "+ arraySize + " numbers.");
        float numberArray[] = new float[arraySize];

        for(int i = 0; i < arraySize; i++)
        {
        System.out.print("Enter num " + (i+1) + ": " );
        while(!sc.hasNextFloat()){
            System.out.println("Not a valid number!");
            sc.next();
            System.out.print("\nPlease try again. Enter num " + (i+1) + ": ");
        }
        numberArray[i] =sc.nextFloat();
        sum = sum +numberArray[i];
        }
        average = sum/arraySize;

        //Display array
        System.out.print("\nNumbers entered: ");
        for(int i =0; i < numberArray.length; i++ ){
            System.out.print(numberArray[i] + " "); 
        }
        printNums(sum,average);
        
    }
    public static void  printNums(float sum, float average) {

        System.out.println("\nSum: "+ String.format("%.2f", sum));
        System.out.println("Average: " + String.format("%.2f", average));
        
    }
}