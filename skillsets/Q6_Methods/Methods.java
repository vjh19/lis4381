import java.util.*;

public class Methods
{
static int age;
static String name;
    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program prompts the user for thier first name and age. It then prints the results.");
        System.out.println("Program uses two method calls to print results. One is a void method and one is a return method.");
        System.out.println();
    }
   public static void getUserInput()
    { 
    Scanner sc = new Scanner(System.in);
    System.out.print("Enter first name: ");
    name = sc.next();
    System.out.print("Enter age: ");
    age = sc.nextInt();
    System.out.println();
       myVoidMethod();
       System.out.println(myValueReturningMethod(name, age));
        
        
    sc.close();
    }
    
    public static void myVoidMethod(){
        System.out.println("void method call: "+ name +" is " + age);
    }
    public static String myValueReturningMethod(String name, int age)
    {
        String message = ("value-returning method call: " + name +" is "+ age);
        return message;
    }
}

 
    
