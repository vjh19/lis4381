import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program searches user-entered integer w/in array of integers.");
        System.out.println("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");
        System.out.println();
    }
   //nonvalue-returning method (without object - static)
    public static void evaluateArray()
    {   int [] number = {3,2,4,99,-1,-5,3,7};
        int val;
        System.out.println("Array Length: " + number.length);
        System.out.print("Enter search value: ");
        Scanner sc = new Scanner(System.in);
        val = sc.nextInt();
        System.out.println();


       for(int i=0; i < number.length; i++)
       {
           if(number[i] == val){
            System.out.println(val + " is found at index " + i);
           }
           else {
            System.out.println(val + " is *not* found at index "+ i);
           }
       }
       System.out.println();
      sc.close();    

      
    }
}
