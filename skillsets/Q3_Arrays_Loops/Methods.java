import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program loops through array of strings.");
        System.out.println("Use following values: dog, cat, bird, fish, insect.");
        System.out.println("Use following loop structures: for, enhance for, while, do...while.");
        System.out.println("Note: Pretest loops: for, enhanced for, while. Posttest loop: do...while.");

        System.out.println();
    }

    public static void evaluateLoop()
    {
        String[] val = {"dog", "cat", "bird", "fish", "insect"};
        System.out.println("for loop:");
        for(int i=0;i<=4;i++){
            System.out.println(val[i]);  
        }
        System.out.println();

        System.out.println("Enhanced for Loop:");
        for( String values: val)
        {
            System.out.println(values);
        }
        System.out.println();

        System.out.println("while loop:");
        int f=0;
        while(f<5)
        {
            System.out.println(val[f]);
            f++;
        }
        System.out.println();

        System.out.println("do...while loop:");
        int z = 0;
        do
        {
        System.out.println(val[z]);
         z++;
        }while(z<5);
        System.out.println();
    }
}