import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program populates Arraylist of strings with user-entered animal type values.");
        System.out.println("Examples: Polar bear, Guinea pig, dog, cat, bird.");
        System.out.println("Program continues to collect user-entered values until user types \"n\".");
        System.out.println("Program displays ArrayList values after each iteration, as well as size.");

        System.out.println();
    }

    public static void evaluateLoop()
    {   
        ArrayList<String> arrayList = new ArrayList<String>();
        Scanner sc = new Scanner(System.in);
        char answer;
        do{
        System.out.print("Enter animal type: ");
        arrayList.add(sc.nextLine());
        System.out.println("ArrayList elements: "+ arrayList);
                        System.out.println("ArrayList size: "+ arrayList.size());
        
                        System.out.print("\nContinue? Enter y or n: ");
        answer = sc.nextLine().charAt(0);
            if(answer=='n'){
                System.out.println();
                        break;
                    }
            else if(answer=='y'){
                continue;
            }
            else{
                System.out.print("Not a valid input. Continue? Enter y or n: ");
                answer = sc.nextLine().charAt(0);
                }
       
        }while(answer=='y');
        
 sc.close();
 
    }
}