

    /*
Note: No need to import file (or package) when file riesides in same directory!
Package(e.g.,folder in directory)
used\to group realted classes and interfaces, and avoid name conflicts
*/

public class Main {
    public static void main(String[] args) 
    {
        //call static methods (i.e., no obejcts)
        Methods.getRequirements();
        
        Methods.evaluateLoop();
    }
}

