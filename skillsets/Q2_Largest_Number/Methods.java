import java.util.*;

public class Methods
{

    //Create Method without returning any value (wihtout object)
    public static void getRequirements()
    {
        //display operational messages
        System.out.println("Developer: Victoria Hagen");
        System.out.println("Program evaluates largest of two integers");
        System.out.println("Note: Program does *not* check for non-integer values.");

        System.out.println();
    }
   //nonvalue-returning method (without object - static)
    public static void evaluateNumber()
    {
        //initialize variables, create Scanner object, capture user input
        int num, num2;
        System.out.print("Enter first integer: ");
        Scanner sc = new Scanner(System.in);
        num = sc.nextInt();
        System.out.print("Enter second integer: ");
        num2 = sc.nextInt();
        sc.close();

        System.out.println();
        if(num > num2)
        {
            System.out.println(num + " is larger than "+ num2);
        }
        if(num2 > num)
        {
            System.out.println(num2 + " is larger than "+ num);
        }
        else{
            System.out.println("Integers are equal.");
        }
    }
}