> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Victoria Hagen

### Assignment 3 Requirements:

*Five Parts:*

1. Create ERD based upon buisness rules
4. Provide DB resource links
5. Create My Event App
4. Database and app screenshots
5. Skillset screenshots 4-6


#### README.md file should include the following items:

* Screenshot of completed ERD
* Screenshots of 10 records for each table
* Screenshots of My Event first and second user interface
* Screenshots of SS4-SS6
* Links to DB resources 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:
| *Screenshot of ERD*:  | *Screenshot of petstore table*:  |
|---|---|
| ![Screenshot ERD](img/ERD.png)  | ![Pestore table](img/petstore.png)  |

| *Screenshot of pet table*:  | *Screenshot of customer table*:  |
|---|---|
| ![Pet table](img/pet.png)  | ![Customer table](img/customer.png)  |Scree

| *Screenshot My Event App first user interface*:  | *Screenshot My Event App second user interface*:  |
|---|---|
| ![Screenshot My Event App first user interface](img/interface1.png)  | ![My Event App second user interface](img/interface2.png)  |

| *Screenshot of SS4*:  | *Screenshot of SS5*:  |   
|---|---|
| ![SS4 Screenshot](img/ss4.png)  | ![SS5 Screenshot](img/ss5.png)  |   

| *Screenshot of SS6*:  | *Screenshot of Extra Credit Random Arrays*:  |
|---|---|
| ![SS6 Screenshot](img/ss6.png)  | ![Random Arrays Screenshot](img/randomarray.png)  |


#### DB Link:

*mwb and sql files:*
[DB Link](https://bitbucket.org/vjh19/lis4381/src/master/a3/docs/ "DB Links")