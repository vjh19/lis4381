<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Assignment 3 items from Bitbucket">
		<meta name="author" content="Victoria Hagen">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>
		<style type="text/css">
		body{
			background-color: #D0A2E4;

		}
		p{
			color: #520A50; 
			text-align: left;
		}
		
		</style>				
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
				<p><strong>Requirements:</Strong>  Create a database and My Event app. App must have launcher icon, activity control color(s), border around image and button and text shaodw (button).</p>
				<p>Frequently, not only will you be asked to design and develop Web applications, but you will also be asked 
				to create (design) database solutions that interact with the Web application—and, in fact, the data repository is the *core* of all Web applications. Hence, the following business requirements.</p>
				<p>A pet store owner, who owns a number of pet stores, requests that you develop a Web application whereby he and his team can record, track, and maintain relevant company data, based upon the following business rules:</p>
 
				<ul style="color: #520A50; text-align: left;">    1. A customer can buy many pets, but each pet, if purchased, is purchased by only one customer. </ul>
				<ul style="color: #520A50; text-align: left;">    2. A store has many pets, but each pet is sold by only one store. </ul>
				
				<p><Strong>Remember:</strong> an organization’s business rules are the key to a well-designed database.</p>
				
				<p>For the Pet's R-Us business, it's important to ask the following questions to get a better idea of how the database and Web application should work together:</p>
				
				
				<ul style="color: #520A50; text-align: left;">  • Can a customer exist without a pet? Seems reasonable. Yes. (optional)</ul>
				<ul style="color: #520A50; text-align: left;">  • Can a pet exist without a customer? Again, yes. (optional)</ul>
				<ul style="color: #520A50; text-align: left;">  • Can a pet store not have any pets? It wouldn’t be a pet store. (mandatory)</ul>
				<ul style="color: #520A50; text-align: left;">  • Can a pet exist without a pet store? Not in this design. (mandatory)</ul> 

				<p><Strong>DB Links:<Strong></p>
					<ul style="color: #520A50; text-align: left;"> • <a href="docs/a3.mwb">a3.mwb</a></ul>
					<ul style="color: #520A50; text-align: left;"> • <a href="docs/a3.sql">a3.sql</a></ul>
			</p>
				
				<h4 style="color: #520A50;">Petstore ERD</h4>
				<img src="img/ERD.png" class="img-responsive center-block" alt="petstore ERD">

				<h4 style="color: #520A50;">Petstore Table</h4>
				<img src="img/petstore.png" class="img-responsive center-block" alt="petstore table">
				
				<h4 style="color: #520A50;">Pet Table</h4>
				<img src="img/pet.png" class="img-responsive center-block" alt="pet store table">

				<h4 style="color: #520A50;">Customer Table</h4>
				<img src="img/customer.png" class="img-responsive center-block" alt="customer table">

				<h4 style="color: #520A50;">Screenshot My Event App first user interface</h4>
				<img src="img/interface1.png" class="img-responsive center-block" alt="Splash Screen">

				<h4 style="color: #520A50;">Screenshot My Event App second user interface</h4>
				<img src="img/interface2.png" class="img-responsive center-block" alt="Calculation Screen">

				<h3 style="color: #520A50;">Skillset Screenshots:</h3>

				<h4 style="color: #520A50;">SS4</h4>
				<img src="img/ss4.png" class="img-responsive center-block" alt="ss4">
				
				<h4 style="color: #520A50;">SS5</h4>
				<img src="img/ss5.png" class="img-responsive center-block" alt="ss5">

				<h4 style="color: #520A50;">SS6</h4>
				<img src="img/ss6.png" class="img-responsive center-block" alt="ss6">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
