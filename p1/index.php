<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="Project 1 from Bitbucket">
		<meta name="author" content="Victoria Hagen">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>
		<style type="text/css">
		body{
			background-color: #D0A2E4;

		}
		p{
			color: #520A50;
		}
		h4{
			color: #520A50;
		}
		</style>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> Create a buisness card with your own photo, contact info, and interests. Must create a launcher icon image and display it in both activities, add backgorund colors, add border around button and image, and add text shadow (button).
				</p>

				<h4>My Buisness Card! Main Page</h4>
				<img src="img/resume1.png" class="img-responsive center-block" alt="Main Page">

				<h4>My Buisness Card! Details Page</h4>
				<img src="img/resume2.png" class="img-responsive center-block" alt="Details Page">

				<h3 style="color: #520A50;">Skillset Screenshots:</h3>

				<h4 style="color: #520A50;">SS7</h4>
				<img src="img/ss7.png" class="img-responsive center-block" alt="ss7">
				
				<h4 style="color: #520A50;">SS8</h4>
				<img src="img/ss8.png" class="img-responsive center-block" alt="ss8">

				<h4 style="color: #520A50;">SS9</h4>
				<img src="img/ss9.png" class="img-responsive center-block" alt="ss9">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
