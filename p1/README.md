> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Victoria Hagen

### Project 1 Requirements:

*Five Parts:*

1. Create My Buisness Card! Android app
2. Must include your photo, contact information, interests, launcher icon, border (button and image), color to activities, and text shadow (button) 
3. Provide screenshots of completed app
4. Provide screenshots of skillsets 7-9
5. Chapter Questions (Chs. 7,8)

#### README.md file should include the following items:

* Screenshot of wokring My Buisness Card! App Main Page
* Screenshot of wokring My Buisness Card! App Details Page
* Screenshots of SS7-SS9

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot My Buisness Card! Main Page*:  | *Screenshot My Buisness Card! Details Page*:  |
|---|---|
| ![My Buisness Card! Main Screenshot](img/resume1.png)  | ![My Buisness Card! Details Screenshot](img/resume2.png)  |

| *Screenshot of SS7*:  | *Screenshot of SS8*:  |
|---|---|
| ![SS7 Screenshot](img/ss7.png)  | ![SS8 Screenshot](img/ss8.png)  |

| *Screenshot of SS9*:  | *Screenshot of Extra Credit Rain Detector:*|
|---|---|
| ![SS9 Screenshot](img/ss9.png)  | ![Rain Detector Screenshot](img/raindetector.png)|
