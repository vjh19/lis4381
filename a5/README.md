> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4381

## Victoria Hagen

### Assignment 5 Requirements:

*Three items:*

1. Use client-side validation from a4 index.php to create add_petstore.php and turn off client-side valdiation.
2. Add server-side validation and regular expressions.
3. Include screenshots of a before and after successful add and failed validation.

#### README.md file should include the following items:

* Screenshots of a before and after successful add and failed validation.
* Screenshots of skillsets 13-15.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshot of index.php:*

![A5 index.php](img/index.png)

| *Invalid add_petstore.php*:  | *Failed Validation add_petstore_process.php*:  | 
|---|---|
| ![Invalid](img/invalid.png)  |  ![Failed Valdidation](img/failed.png)  |

| *Valid add_petstore.php*:  | *Passed Validation add_petstore_process.php*:  | 
|---|---|
| ![Valid](img/valid.png)  |  ![Passed Valdidation](img/passed.PNG)  |

*Screenshot of SS13:*

![ss13](img/ss13.PNG)

| *SS14 Addition index.php*:  | *SS14 Addition Result*:  |  *SS14 Division index.php*: | *SS14 Divide by Zero Result*:|
|---|---|---|---|
| ![Addition](img/ss14_add.PNG)  | ![Addition Result](img/ss14_addition.PNG)  |    ![Division](img/ss14_divide.PNG)  |![Division Result](img/ss14_divide0.PNG)  |

| *Write/Read File index.php*:  | *Failed Validation add_petstore_process.php*:  | 
|---|---|
| ![index.php](img/ss15_index.PNG)  |  ![process.png](img/ss15_process.PNG)  |

#### Site Link:

*Portfolio Site:*
[Victoria Hagen Portfolio](http://localhost/repos/lis4381/index.php "Portfolio site")