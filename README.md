> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Victoria Hagen

### LIS4381 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")

    - Install AMPPS
    - Install JDK
    - Install Android Studio and Create My First App
    - Provide screenshots of installations
    - Create Butbucket repo
    - Complete Bitbucket tutorials (bitbucketstationlocations)
    - Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")

    - Create Healthy Recipes Android app
    - Provide screenshots of completed app
    - Provide screenshots of skillsets 1-3

3. [A3 README.md](a3/README.md "My A3 README.md file")

    - Create ERD based upon buisness rules
    - Provide DB resource links
    - Create My Event App
    - Database and app screenshots
    - Skillset screenshots 4-6

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Clone assignment starter files.
    - Modify meta tags, titles, naviagation links, and header tags appropriately.
    - Add form controls to match attributes of petstore entity, and add jQuery validation and regular expressions.
    - Create a favicon and place it in each assignments main directory.
    - Put associated Bitbucket content in each index.php file for A1, A2, A3, and P1 working links.
    
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Use client-side validation from a4 index.php to create add_petstore.php and turn off client-side valdiation.
    - Add server-side validation and regular expressions.
    - Include screenshots of a before and after successful add and failed validation.

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Create My Buisness Card! Android app
    - Provide screenshots of completed app
    - Provide screenshots of skillsets 7-9
7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Create an edit_petstore.php, edit_petstore_process.php and delete_petstore.php after duplicated sever-side validation from A5.
    - Add server-side validation and regular expressions.
    - Include screenshots of a before and after successful edit, failed validation, delete prompt, and successfully deleted record.
