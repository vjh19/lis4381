<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="All assingment one items from Bitbucket.">
		<meta name= "author" content="Victoria Hagen.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 1</title>		
		<?php include_once("../css/include_css.php"); ?>
		
		<style type="text/css">
		body{
			background-color: #D0A2E4;

		}
		
		</style>
			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p style="color: #520A50;"class="text-justify">
					<strong>Description:</strong> Development installations of JDK, Andorid Studio and AMPPS. Create "My First App!".
				</p>

				<h4 style="color: #520A50;">Java Installation</h4>
				<img src="img/jdk_install.png" class="img-responsive center-block" alt="JDK Installation">

				<h4 style="color: #520A50;">Android Studio Installation</h4>
				<img src="img/android.png" class="img-responsive center-block" alt="Android Studio Installation">

				<h4 style="color: #520A50;">AMPPS Installation</h4>
				<img src="img/ampps.png" class="img-responsive center-block" alt="AMPPS Installation">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
