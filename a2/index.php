<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="All assingment two items from Bitbucket.">
		<meta name="author" content="Victoria Hagen">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>
		<style type="text/css">
		body{
			background-color: #D0A2E4;

		}
		
		</style>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p style="color: #520A50;"class="text-justify">
					<strong>Description:</strong> Create Healthy Recipes Android app and provide screenshots of completed app.
				</p>

				<h4 style="color: #520A50;">Healthy Recipes Main Page</h4>
				<img src="img/main.png" class="img-responsive center-block" alt="Healthy Recipes Main Page">

				<h4 style="color: #520A50;">Healthy Recipes Recipe Page</h4>
				<img src="img/recipe.png" class="img-responsive center-block" alt="Healthy Recipes Recipe Page">

				<h3 style="color: #520A50;">Skillset Screenshots:</h3>

				<h4 style="color: #520A50;">SS1</h4>
				<img src="img/ss1.png" class="img-responsive center-block" alt="ss1">
				
				<h4 style="color: #520A50;">SS2</h4>
				<img src="img/ss2.png" class="img-responsive center-block" alt="ss2">

				<h4 style="color: #520A50;">SS3</h4>
				<img src="img/ss3.png" class="img-responsive center-block" alt="ss3">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
