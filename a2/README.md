> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 - Mobile Web Application Development

## Victoria Hagen

### Assignment 2 Requirements:

*Four Parts:*

1. Create Healty Recipes Android app
2. Provide screenshots of completed app
3. Provide screenshots of skillsets 1-3
4. Chapter Questions (Chs. 3,4)

#### README.md file should include the following items:

* Screenshot of wokring Healthy Recipes App Main Page
* Screenshot of wokring Healthy Recipes App Recipe Page
* Screenshots of SS1-SS3

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

| *Screenshot Healthy Recipes Main Page*:  | *Screenshot Healthy Recipes Recipe Page*:  |
|---|---|
| ![Healthy Recipes Main Screenshot](img/main.png)  | ![Healthy Recipes Recipe Screenshot](img/recipe.png)  |

| *Screenshot of SS1*:  | *Screenshot of SS2*:  | *Screenshot of SS3*:  |
|---|---|---|
| ![SS1 Screenshot](img/ss1.png)  | ![SS2 Screenshot](img/ss2.png)  | ![SS3 Screenshot](img/ss3.png)  |









