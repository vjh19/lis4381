	<nav class="navbar navbar-light navbar-fixed-top"style="background-color: #520A50;">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand"style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;"  href="https://bitbucket.org/vjh19/lis4381/src/master/" target="_blank" ><img src="img/ic_launcher.png" alt="Bitbucket"/></a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="../index.php">LIS4381</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a1/index.php">A1</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a2/index.php">A2</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a3/index.php">A3</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a4/index.php">A4</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="a5/index.php">A5</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="p1/index.php">P1</a></li>
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="p2/index.php">P2</a></li>	
					<li><a style="color: #fff; text-shadow: 1.25px 1.25px 2px #DDB6DC; font-style: bold;" href="test/index.php">Test</a></li>				
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	
