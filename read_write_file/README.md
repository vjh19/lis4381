> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# Course LIS4381

## Vicotria Hagen

### Assignment 4 Requirements:

*Five items:*

1. Clone assignment starter files.
2. Modify meta tags, titles, naviagation links, and header tags appropriately.
3. Add form controls to match attributes of petstore entity, and add jQuery validation and regular expressions.
4. Create a favicon and place it in each assignments main directory.
5. Put associated Bitbucket content in each index.php file for A1, A2, A3, and P1 working links.

#### README.md file should include the following items:

* Sreenshots of main page and both failed and passed valdiations.
* Screenshots of SS10-SS12.
* Link to local LIS4381 web app.


#### Assignment Screenshots:

| *LIS4381 Portal (Main Page)*:  |  |
|---|---|
| ![LIS4381 Portal](img/lis4381page.PNG)  |  

| *Passed Validation*:  | *Failed Validation*:  | 
|---|---|
| ![Passed Validation](img/validation.PNG)  |  ![Failed Valdidation](img/failed_validation.PNG)  |

| *Screenshot of SS10*:  | *Screenshot of SS11*:  |  *Screenshot of SS12*: |
|---|---|---|
| ![SS10 Screenshot](img/ss10.PNG)  | ![SS11 Screenshot](img/ss11.PNG)  |    ![SS12 Screenshot](img/ss12.PNG)  |


#### Site Link:

*Portfolio Site:*
[Victoria Hagen Portfolio](http://localhost/repos/lis4381/index.php "Portfolio site")
