<?php
ini_set('display_errors',1);
//ini_set('log_errors',1);
//ini_set('error_log', dirname(__FILE__) . '/error_log.txt');
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Process page for Read/Write File">
	<meta name="author" content="Victoria Hagen">
	<link rel="icon" href="favicon.ico">

	<title>Write Read File</title>
		<?php include_once("../css/include_css.php"); ?>
		<style type="text/css">
		body{
			background-color: #D0A2E4;

		}
		h2{
			color: #520A50;
		}
		label{
			color: #520A50;
		}
		</style>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<p class="text-justify">
					<?php
					//create/open file for appending write/read
					$myfile = fopen("file.txt", "w+") or exit("Unable to open file!");
					$txt=$_POST['comment'];
					fwrite($myfile, $txt);
					fclose($myfile);

					$myfile = fopen("file.txt", "r+") or exit("Unable to open file!");

					while(!feof($myfile)){
						echo fgets($myfile) . "<br />";
					}
					fclose($myfile);
					?>
					</p>

			<?php include_once "global/footer.php"; ?>
			
		</div> <!-- end starter-template -->
 </div> <!-- end container -->
<script>
	$(document).ready(function()){
		$('#myTable').DataTable({
			responsive: true
	});
	});
	</script>
	
</body>
</html>
